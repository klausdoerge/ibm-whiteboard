# Whiteboard

IBM-CIC hiring test reply

Details

- C#/.Net 5 Solution
- Requires MSSQL Server
- Uses simple JWT access token 
- Role: Senior Developer - IBM CIC
 
## Technology justification
The .Net framework is a modern and testet framework both suitable for developing MVP's and full scale solutions. 

## Details
It was not clear from the recruiter what the job specifically was focused on, so I focused on the backend for the solution described in the test.

I did a basic raw setup of an API for the Whiteboard application. I imagine this - when completed - could be easily hooked up to a frontend SPA.